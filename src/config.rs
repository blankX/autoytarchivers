use std::time::Duration;

pub const API_ID: i32 = 0;
pub const API_HASH: &str = "https://my.telegram.org";
pub const BOT_TOKEN: &str = "https://telegram.org/BotFather";

// To obtain the packed chat, set to None and send a message to the chat
pub const STORAGE_CHAT: Option<&[u8]> = None;
pub const STORAGE_MESSAGE_ID: i32 = 7;
pub const WAIT_DURATION: Duration = Duration::from_secs(30 * 60);
pub const VIDEO_WORKERS: usize = 2;
pub const UPLOAD_WORKERS: usize = 5;

pub const CHANNEL_IDS: [&str; 2] = [
    "UCL_qhgtOy0dy1Agp8vkySQg",
    "UCHsx4Hqa-1ORjQTh9TYDhww",
];
pub const INVIDIOUS_INSTANCES: Option<[&str; 6]> = Some([
    "https://tube.connect.cafe",
    "https://invidious.zapashcanon.fr",
    "https://invidious.site",
    "https://invidious.048596.xyz",
    "https://vid.puffyan.us",
    "https://invidious.silkky.cloud",
]);
